// Mini LiveCycle Simulator
// MiLiCySi
//based on Cat and Mouse Simulator

import game2dai.entities.*;
import game2dai.entityshapes.ps.*;
import game2dai.maths.*;
import game2dai.*;
import game2dai.entityshapes.*;
import game2dai.fsm.*;
import game2dai.steering.*;
import game2dai.utils.*;
import game2dai.graph.*;

final float CAT_WANDER_SPEED  = 70;  
final float CAT_CHASE_SPEED    = 160;  
final float CAT_SEEK_SPEED    = 110;

World world;
StopWatch sw;
String legend;
float legendOffsetX;

BitmapPic view;
Marten marten;

void setup() {
  size(600, 600);
   world = new World(width, height);
   textSize(12);
  legend = "HINTS [1]-Heading  [2]-Velocity  [3]-Wander  [4]-View  [5]-Obstacle detection  [0]-All off";
  legendOffsetX = (width - textWidth(legend))/2;
  
  createMarten();
  
  reset();
  sw = new StopWatch();
  }

void draw() {
  background(200, 230, 200);
  double elapsedTime = sw.getElapsedTime();
  world.update(elapsedTime);
  fill(0);
  text(legend, legendOffsetX, height - 6);
  world.draw(elapsedTime);
  
}

public void createMarten() {
  marten = new Marten(Vector2D.ZERO, // position
  20, // collision radius
  Vector2D.ZERO, // velocity
  CAT_WANDER_SPEED, // maximum speed
  Vector2D.random(null), // heading
  1.5, // mass
  2.5f, // turning rate
  2500 // max force
  ); 
  Domain catDomain = new Domain(12, 12, width-12, height-12);
  marten.worldDomain(catDomain, SBF.REBOUND);
  marten.viewFactors(260, PApplet.TWO_PI/7);
  //marten.renderer(new CatPic(this, 40));
  marten.AP().wanderFactors(100, 40, 20);
  marten.AP().obstacleAvoidDetectBoxLength(40);
  view = new BitmapPic(this, "tanks32_4.png", 8, 1);
  float newInterval = map(10, 0, 10, 0.6f, 0.04f);
  view.setAnimation(newInterval, 1);
  marten.renderer(view);
  world.add(marten);
}

public void reset() {
  // Remove all pending births and deaths
  //    world.cancelBirthsAndDeaths();
  Vector2D pos;
  // Starting position for the cat
  pos = Vector2D.random(null).mult(50);
  println(pos);
  marten.moveTo(pos.x + 150, pos.y + 150);
  marten.velocity(10, 20);
  marten.AP().obstacleAvoidOn();
  //marten.FSM().changeState(catWanderState);
 // marten.miceKilled = 0;
  world.add(marten);
  // Starting positions for mice
  int[] dx = { 
    150, 450, 450
  };
  int[] dy = { 
    450, 450, 150
  };

}
