public class Marten extends Inhabitant {

    public Marten(
                    Vector2D position, 
                    double radius, 
                    Vector2D velocity, 
                    double max_speed, 
                    Vector2D heading, 
                    double mass, 
                    double max_turn_rate, 
                    double max_force
                    ) 
    {
        super(position, radius, velocity, max_speed, heading, mass, max_turn_rate, max_force);
        addFSM();
    }

}
